package com.micro.camel.usuarios.SoapTesting;

import java.util.ArrayList;

import org.springframework.stereotype.Component;
import org.tempuri.Add;

@Component
public class AddRequestBuilder  {

	public ArrayList<Integer> getAddRequest(int a, int b) {
		ArrayList<Integer> request = new ArrayList<Integer>();
		int num1 = a;
		int num2 = b;
		request.add(num1);
		request.add(num2);
		return request;
		
	}
}
