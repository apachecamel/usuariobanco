package com.micro.camel.usuarios.SoapTesting;

import org.apache.camel.json.simple.JsonObject;
import org.apache.cxf.message.MessageContentsList;
import org.springframework.stereotype.Component;



@Component
public class AddResponseBuilder {

	public JsonObject getAddResponse(MessageContentsList body) {
		JsonObject j = new JsonObject();
		j.put("Value", body.get(0));
		return j;
	}
}
