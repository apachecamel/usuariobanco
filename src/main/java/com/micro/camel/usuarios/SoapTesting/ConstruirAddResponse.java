package com.micro.camel.usuarios.SoapTesting;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.cxf.message.MessageContentsList;
import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.model.ErrorMessage;

@Component
public class ConstruirAddResponse implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		
		//extrae la respuesta del servicio
		MessageContentsList response = exchange.getIn().
							getBody(MessageContentsList.class);
		
		//construye los elementos de respuesta
		Integer a = (Integer) response.get(0);
		ErrorMessage em = new ErrorMessage();
		//devuelve el msj en formato Json
		em.setError(a);
		em.setMessage("Se encontro el numero :"+a);
		exchange.getIn().setBody(em);
	}

}
