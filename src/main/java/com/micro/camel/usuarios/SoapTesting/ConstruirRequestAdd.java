package com.micro.camel.usuarios.SoapTesting;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.springframework.stereotype.Component;

@Component
public class ConstruirRequestAdd implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		//Obtiene los parametros del Header
		int a = Integer.parseInt(exchange.getIn().getHeader("a").toString());
		int b = Integer.parseInt(exchange.getIn().getHeader("b").toString());
		//Arma la lista de parametros
		final List<Integer> params = new ArrayList<Integer>();
		params.add(a);
		params.add(b);
		//Carga el Exchange
		exchange.getIn().setBody(params);
		exchange.getIn().setHeader(CxfConstants.OPERATION_NAME, "Add");
		
	}

}
