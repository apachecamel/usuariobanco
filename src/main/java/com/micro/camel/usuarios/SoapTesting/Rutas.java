package com.micro.camel.usuarios.SoapTesting;

import org.apache.camel.BeanInject;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.camel.json.simple.JsonObject;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;
import org.tempuri.CalculatorSoap;

import com.micro.camel.usuarios.exceptions.OperationInvalidException;
import com.micro.camel.usuarios.rutas.ErrorHandlerRoute;

/**
 * Ruta de testeo para llamar un servicio SOAP
 * "/soap/{Operacion}/a/{a}/b/{b}"
 * Para realizar las distintas operaciones:
 * add = suma
 * sub = resta
 * mul = multiplica
 * div = divide
 * @author santiago.ferreiro
 *
 */
@Component
public class Rutas extends ErrorHandlerRoute {
	
	String getAddResquest = "getAddRequest(${in.headers.a},${in.headers.b})";
	String selectOP = "direct:selectOperation";
	
	@BeanInject
	CxfEndpoint endpointCalculadora;
	
	@BeanInject
	ConstruirRequestAdd addrequest;
	
	@BeanInject
	ConstruirAddResponse addresponse;
	
	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		super.configure();
		
		rest("/soap")
		.get("/{operacion}/a/{a}/b/{b}")
		.route()
		.log("A: ${in.headers.a}")
		.log("B: ${in.headers.b}")
		
		//preparar el Exchange para enviar el msj soap
		.bean(AddRequestBuilder.class, getAddResquest)
		
		//ChoiceBLock
		.to(selectOP)
		
		//LLama al servicio
		.to(endpointCalculadora)
		
		
		//Interpreta la respuesta y la transofrma en un Json Legible
		.bean(AddResponseBuilder.class)
		.log("Service Response: ${body}")
		.marshal().json(JsonLibrary.Jackson)
		.unmarshal().json(JsonLibrary.Jackson, JsonObject.class)
		.endRest()
		
		
		.get("/info")
		.route()
		.setBody(constant("/soap/{Operacion}/a/{a}/b/{b}" 
				+"Para realizar las distintas operaciones:"+  
				"add = suma / "+
				"sub = resta / "+
				"mul = multiplica / "+
				"div = divide"))
		.endRest();

	
		from(selectOP)
		//Decide la operacion segun Header ${operacion}
				.choice()
					.when().simple("${in.headers.operacion} == 'add'")
						.setHeader(CxfConstants.OPERATION_NAME).simple("Add")
					.endChoice()
					.when().simple("${in.headers.operacion} == 'sub'")
						.setHeader(CxfConstants.OPERATION_NAME).simple("Subtract")
					.endChoice()
					.when().simple("${in.headers.operacion} == 'mul'")
						.setHeader(CxfConstants.OPERATION_NAME).simple("Multiply")
					.endChoice()
					.when().simple("${in.headers.operacion} == 'div'")
						.setHeader(CxfConstants.OPERATION_NAME).simple("Divide")
					.endChoice()
						.otherwise().throwException(OperationInvalidException.class, "Operacion Invalida, elija otra")
					.endChoice()
				.end();
	}
	
}
