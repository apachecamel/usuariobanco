package com.micro.camel.usuarios.model;

public final class Constantes {
	
	public static final String PLACEHOLDER = "This is a Place Holder message. Change it ASAP!";

	public static final class Errores {

		public static final String ERROR_GENERICO = "Error generico!";
		public static final String ERROR_GENERICO_NO_CAPTADO = "Error generico no captado!";
		public static final String USUARIO_NO_ENCONTRADO = "Usuario no encontrado!";
		public static final String FORMATO_INVALIDO = "Formato invalido!";
		public static final String USUARIO_PASSWORD_INCORRECTO = "Combinacion Usuario password no valida.";
		public static final String NUMBER_FORMAT_E = "Formato incorrecto, utilize numeros!";
//		public static final String
		
	}
	
	public static final class Headers {
		public static final String AUTHORIZATION = "authorization";
		public static final String IDBANCO = "in.headers.idbanco";
	}
	
	public static final class RutasId {
		public static final String ActiveMqTimer = "activeMqTimer";
		public static final String ActiveMqTimerReceiver = "activeMqTimerReceiver";
		public static final String ColaUsuariosParaLog = "Log_Users_file";
		public static final String ColasBancos = "User_wait_bank";
		public static final String ColasBancosGalicia = "users_wait_bank_galicia";
		public static final String ColasBancosHSBC = "users_wait_bank_hsbc";
		public static final String ColasBancosBBVA = "users_wait_bank_bbva";
		public static final String ColasBancosDeadLetter = "deadletter_bank";
		public static final String embotellado = "embotellado";
		public static final String embotelladoGalicia = "embotellado_Galicia";
		public static final String embotelladoBBVA = "embotellado_BBVA";
		public static final String embotelladoHSBC = "embotellado_HSBC";
		public static final String capturaUsuariosParaImpactarBdd = "Pre_BDD_queue";
		public static final String ProcesadorBdd = "procesador_Bdd";
	}
	
	public static final class Rutas {
		public static final String ColasBancos = "activemq:queue:users_wait_bank";
		public static final String ColasBancosGalicia = "activemq:queue:users_wait_bank_galicia";
		public static final String ColasBancosHSBC = "activemq:queue:users_wait_bank_hsbc";
		public static final String ColasBancosBBVA = "activemq:queue:users_wait_bank_bbva";
		public static final String ColasBancosDeadLetter = "activemq:queue:deadletter_bank";
		public static final String ColaCuentas = "activemq:queue:cuentas_prebdd";
		public static final String capturaUsuariosParaImpactarBdd = "file:logs/json.txt?fileExist=Append&fileName=LogsJason" 
									+ "Banco ${in.headers.idbanco}"+"/"+"Numero de cuentas ${in.headers.iteracion}";
		public static final String LOGGER = "log:LOGGER?level=INFO";
		public static final String ProcesadorBdd = "direct:procesadorBdd";
		public static final String ArchivoXml = "file:output/xmlSoap";
	}
	
	public static final class Soap{
		public static final class Calculadora{
			public static final String serviceUrl = "http://www.dneonline.com/calculator.asmx";
			public static final String wsdlPathAfuera = "http://www.dneonline.com/calculator.asmx?wsdl";
		}
	}
	
}
