package com.micro.camel.usuarios.model;

public class ErrorMessage {
	
	private String message;
	private int error;
	
	public ErrorMessage() {
		this.error = 1;
		this.message = "Error generico disparado";
	}

	public ErrorMessage(String msg) {
		// TODO Auto-generated constructor stub
		this();
		this.message = msg;
		
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getError() {
		return error;
	}

	public void setError(int error) {
		this.error = error;
	}
	
	

}
