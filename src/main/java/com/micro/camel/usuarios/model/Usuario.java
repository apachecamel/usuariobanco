package com.micro.camel.usuarios.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Usuario {

	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "name")
	private String name;
	@Column(name = "pass")
	private String pass;
	@Column(name = "token")
	private String token;

//	@JsonIgnore
	@ElementCollection(fetch = FetchType.EAGER)
	private Set<Long> cuentas;

	@ElementCollection(fetch = FetchType.EAGER)
	private Set<Long> bancosid;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Usuario() {
		this.token = new Date().toString();
		this.bancosid = new HashSet<Long>();
		this.cuentas = new HashSet<Long>();
	}

	public Set<Long> getCuentas() {
		return cuentas;
	}

	public void setCuentas(Set<Long> cuentas) {
		this.cuentas = cuentas;
	}

	public Set<Long> getBancosid() {
		return bancosid;
	}

	public void setBancosid(Set<Long> bancosid) {
		this.bancosid = bancosid;
	}

	public void addBanco(Long bancoid) {
		if(this.bancosid.contains(bancoid)){
			System.out.println("Banco ya ingresado");
		}else
		this.bancosid.add(bancoid);
	}

	public void addCuenta(Long idcuenta) {
		if(this.cuentas.contains(idcuenta)){
			System.out.println("Cuenta ya ingresado");
		}else
		this.cuentas.add(idcuenta);
		
	}
	
	public Usuario(String name, String pass) {
		this();
		this.name=name;
		this.pass=pass;
	}
}
