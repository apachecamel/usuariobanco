package com.micro.camel.usuarios.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.model.Usuario;
@Component
public class AddBanco implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub

		System.out.println("Headers del Exchange: ");
		System.out.println("Id del Banco: "+exchange.getIn().getHeader("idbanco"));
	
		Usuario usu = exchange.getIn().getBody(Usuario.class);
		usu.addBanco(Long.parseLong(exchange.getIn().getHeader("idbanco").toString()));
	}

}
