package com.micro.camel.usuarios.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.model.Usuario;

@Component
public class AddCuenta implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception {

		Usuario usu = exchange.getIn().getBody(Usuario.class);	
		usu.addCuenta(Long.parseLong(exchange.getIn().getHeader("cuentaid").toString()));
	}

}


