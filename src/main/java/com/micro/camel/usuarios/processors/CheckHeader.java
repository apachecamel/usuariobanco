package com.micro.camel.usuarios.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

@Component
public class CheckHeader implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String text = exchange.getIn().getHeader("id").toString();
		if (text.matches("[0-9]+")) {
			exchange.getIn().setHeader("type", "Long");
		} else {
			exchange.getIn().setHeader("type", "String");
		}
	}

}
