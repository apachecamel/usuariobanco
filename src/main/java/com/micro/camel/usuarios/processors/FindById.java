package com.micro.camel.usuarios.processors;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.exceptions.userNotFoundException;
import com.micro.camel.usuarios.model.Usuario;
import com.micro.camel.usuarios.repository.UsuarioDao;

@Component
@DependsOn("RouteConfig")

public class FindById implements Processor {
	
	@BeanInject
	private UsuarioDao usudao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("Id de la cuenta: "+exchange.getIn().getHeader("id"));
		
		Usuario usu = usudao.findById(
				Long.parseLong(
				exchange.getIn().getHeader("id")
					.toString())).orElse(null);
		if(usu==null)
			throw new userNotFoundException();
		else
			exchange.getIn().setBody(usu);
		
	
		
	}

}
