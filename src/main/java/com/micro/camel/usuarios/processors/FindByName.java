package com.micro.camel.usuarios.processors;

import java.util.ArrayList;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.exceptions.userNotFoundException;
import com.micro.camel.usuarios.model.Usuario;
import com.micro.camel.usuarios.repository.UsuarioDao;

@Component
public class FindByName implements Processor{
	
	@BeanInject
	private UsuarioDao usudao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("FindByName");
	
		ArrayList<Usuario> usu = usudao.findByName(
										exchange.getIn().getHeader("id")
													.toString());
		
		if(usu.size() == 0)
			throw new userNotFoundException();
		else
			exchange.getIn().setBody(usu);
	}

}
