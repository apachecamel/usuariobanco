package com.micro.camel.usuarios.processors;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.model.Usuario;
import com.micro.camel.usuarios.repository.UsuarioDao;

@Component
public class SaveEntity implements Processor{
	
	@BeanInject
	private UsuarioDao usudao;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		System.out.println(exchange.getIn().getBody(Usuario.class));
		usudao.save(exchange.getIn().getBody(Usuario.class));
	}
	
	
}
