package com.micro.camel.usuarios.processors;

import java.util.ArrayList;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.exceptions.userNotFoundException;
import com.micro.camel.usuarios.model.Constantes;
import com.micro.camel.usuarios.model.Usuario;
import com.micro.camel.usuarios.repository.UsuarioDao;
import com.micro.camel.usuarios.services.UsuarioDaoService;

import javafx.util.Pair;

@Component
public class Util implements UsuarioDaoService{
	
	@Autowired
	private UsuarioDao usudao;

	public Pair<String, String> intentarBasicAuth(String strBasic) {
		String[] keyVal = strBasic.split(" ");
		if (keyVal.length != 2) {
			throw new RuntimeException(Constantes.Errores.FORMATO_INVALIDO+"1");
		}
		if (!keyVal[0].equals("Basic")) {
			throw new RuntimeException(Constantes.Errores.FORMATO_INVALIDO+"2");
		}
		// Ahora decodificar en base 64 el usuario y la password

		byte[] decodedBytes = Base64.getDecoder().decode(keyVal[1]);
		String usu_pass = new String(decodedBytes);

		// Recordar que el usuario y la password vienen separados por :
		keyVal = usu_pass.split(":");

		if (keyVal.length != 2) {
			throw new RuntimeException(Constantes.Errores.FORMATO_INVALIDO+"3");
		}

		String usuario = keyVal[0];
		String password = keyVal[1];

		return new Pair<String, String>(usuario, password);
	}

	public boolean validar(Pair<String, String> usu_pass) throws Exception {
		ArrayList<Usuario> usu = usudao.findByName(usu_pass.getKey());
//		System.out.println(usu_pass.getKey());
//		for(Usuario usu2 : usu) {
//			System.out.println(usu2.getName());
//		}
		if(usu.isEmpty())
			throw new userNotFoundException();
		else {
			System.out.println("usu.getPass: "+usu.get(0).getPass());
			System.out.println("usu_pass.getValue: "+usu_pass.getValue());
			if(usu.get(0).getPass().equals(usu_pass.getValue()))
				return true;
			else
				return false;
		}
		
	}
	
	public String generarToken() {
		return "Se genera un token. "+Constantes.PLACEHOLDER;
	}


}
