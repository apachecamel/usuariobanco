package com.micro.camel.usuarios.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.micro.camel.usuarios.model.Usuario;

@Repository
public interface UsuarioDao extends JpaRepository<Usuario, Long> {

	public List<Usuario> findByBancosid(Long bancosid);
	public ArrayList<Usuario> findByName(String name);
	
	
}
 