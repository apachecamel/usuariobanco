package com.micro.camel.usuarios.rutas;

import java.util.ArrayList;

import org.apache.camel.Exchange;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.jackson.ListJacksonDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.apache.camel.processor.aggregate.GroupedBodyAggregationStrategy;
import org.apache.camel.spi.DataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.exceptions.BancoNotFoundException;
import com.micro.camel.usuarios.model.Constantes;
import com.micro.camel.usuarios.model.Usuario;
import com.micro.camel.usuarios.processors.AddBanco;

/**
 * Rutas que pasan o invocan a la ActiveMQ
 * Timers, cola de aguantadero de usuario.
 * @author santiago.ferreiro
 *
 */

@Component
public class ActiveMq extends ErrorHandlerRoute {

	public static int counter;
	
	DataFormat jsonTOUsuario = new JacksonDataFormat(Usuario.class);
	DataFormat jsonTOUsuarioList = new ListJacksonDataFormat(Usuario.class);
	
	@Autowired
	private AddBanco addBanco;
	
	public void configure() throws Exception {
		super.configure();
		
		from("seda:tomq")
		.routeId("wiretap to mq")
		.marshal(jsonTOUsuario)
		.to("log:LOGGER?level=INFO&showHeaders=true")
		.wireTap("activemq:queue:usuarios");
		
		 from("timer:cola?fixedRate=true&period=10s")
		 .routeId(Constantes.RutasId.ActiveMqTimer)
		 .autoStartup(false)
		 .setBody(constant("Mensaje: "+counter))
		 .log("Se envia un mensaje a la cola timer.")
		 .to("activemq:queue:timer");
		 
		 from("activemq:queue:timer")
		 .routeId(Constantes.RutasId.ActiveMqTimerReceiver)
		 .autoStartup(false)
		 .log("Se recibe un mensaje de la cola.");
		
		 
		 from("activemq:queue:usuarios")
		 .routeId(Constantes.RutasId.ColaUsuariosParaLog)
		 .autoStartup(false)
		 .log("Vaciando cola!")
		 .log("${body}")
		 .to("file:logs/json.txt?fileExist=Append&fileName=LogsJason");
		 
		 from(Constantes.Rutas.ColasBancos)
		 .autoStartup(false)
		 .routeId(Constantes.RutasId.ColasBancos)
		 .log("${in.header.idbanco}")
		 .unmarshal(jsonTOUsuario)
		 .choice()
		 	.when().simple("${in.header.idbanco} == 1")
		 		.process(addBanco)
		 		.marshal(jsonTOUsuario)
		 		.to(Constantes.Rutas.ColasBancosGalicia)
		 	.when().simple("${in.header.idbanco} == 2")
		 		.process(addBanco)
		 		.marshal(jsonTOUsuario)
		 		.to(Constantes.Rutas.ColasBancosBBVA)
		 	.when().simple("${in.header.idbanco} == 3")
		 		.process(addBanco)
		 		.marshal(jsonTOUsuario)
		 		.to(Constantes.Rutas.ColasBancosHSBC)
		 	.otherwise()
		 		.throwException(BancoNotFoundException.class, "Banco no encontrado u opcion no valida!")

		 .endChoice();

		
		from(Constantes.Rutas.ColasBancosHSBC)
		.routeId(Constantes.RutasId.embotelladoHSBC)
		.autoStartup(false)
		.to(Constantes.Rutas.ColaCuentas);
		
		from(Constantes.Rutas.ColasBancosBBVA)
		.routeId(Constantes.RutasId.embotelladoBBVA)
		.autoStartup(false)
		.to(Constantes.Rutas.ColaCuentas);
		
		from(Constantes.Rutas.ColasBancosGalicia)
		.routeId(Constantes.RutasId.embotelladoGalicia)
		.autoStartup(false)
		.to(Constantes.Rutas.ColaCuentas);
		
		from(Constantes.Rutas.ColaCuentas)
		.autoStartup(false)
		.routeId(Constantes.RutasId.embotellado)
		.log("${body}")
		.aggregate(simple("${"+Constantes.Headers.IDBANCO +"}"), new AggregationStrategy() {
			
			@Override
			public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
				// TODO Auto-generated method stub
				String s = newExchange.getIn().getBody(String.class);
				if (oldExchange==null) {
					int contador = 0;
					newExchange.getIn().setHeader("iteracion", contador);
					return newExchange;
				}else {
					String s2 ="!"+ s.concat(oldExchange.getIn().getBody(String.class));
					int c = (int) oldExchange.getIn().getHeader("iteracion");
					c++;
					newExchange.getIn().setHeader("iteracion", c);
					newExchange.getIn().setBody(s2);
					return newExchange;
				}
			}
		})
		.completionTimeout(1000*10)
		.log("${body}")
		.toD(Constantes.Rutas.capturaUsuariosParaImpactarBdd);
		
		
		
	}
}
