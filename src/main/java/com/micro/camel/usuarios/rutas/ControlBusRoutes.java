package com.micro.camel.usuarios.rutas;

import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.model.Constantes;

@Component
public class ControlBusRoutes extends ErrorHandlerRoute {

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		super.configure();
		
		rest("/filelog")
		.get("/start")
		.route()
		.wireTap("controlbus:route?routeId="
		+Constantes.RutasId.ColaUsuariosParaLog+"&action=start")
		.to("log:LOGGER?showAll=true&multiline=true")
		.setBody(constant("Se inicio la ruta: "
		+Constantes.RutasId.ColaUsuariosParaLog))
		.endRest()
		
		.get("/stop")
		.route()
		.wireTap("controlbus:route?routeId="
		+Constantes.RutasId.ColaUsuariosParaLog+"&action=stop")
		.to("log:LOGGER?showAll=true&multiline=true")
		.setBody(constant("Se detuvo la ruta: "
		+Constantes.RutasId.ColaUsuariosParaLog))
		.endRest();
		
		rest("/queue-banco-control")
		
		.get("/stop")
		.route()
		.wireTap("controlbus:route?routeId="
		+Constantes.RutasId.ColasBancos+"&action=stop")
		.setBody(constant("Se detuvo la ruta: "
				+Constantes.RutasId.ColasBancos))
		.endRest()
		
		.get("/stats")
		.route()
		.wireTap("controlbus:route?routeId="
		+Constantes.RutasId.ColasBancos+"&action=stats")
		.log("${body}")
		.log("${headers}")
		.setBody(constant("Se pidieron los stats de la ruta: "
				+Constantes.RutasId.ColasBancos))
		.to("file:logs/xml?fileExist=Append&fileName=controlBusStats")
		.endRest()
		
		
		.get("/start")
		.route()
		.wireTap("controlbus:route?routeId="
		+Constantes.RutasId.ColasBancos+"&action=start")
		.setBody(constant("Se inicio la ruta: "
				+Constantes.RutasId.ColasBancos))
		.endRest();
		
rest("/queue-embotellado-control")
		
		.get("/stop")
		.route()
		.wireTap("controlbus:route?routeId="
		+Constantes.RutasId.embotelladoBBVA+"&action=stop")
		.wireTap("controlbus:route?routeId="
				+Constantes.RutasId.embotelladoGalicia+"&action=stop")
		.wireTap("controlbus:route?routeId="
				+Constantes.RutasId.embotelladoHSBC+"&action=stop")
		.setBody(constant("Se detuvieron las rutas: "
				+Constantes.RutasId.embotelladoBBVA+" "
				+Constantes.RutasId.embotelladoHSBC+" "
				+Constantes.RutasId.embotelladoGalicia))
		.endRest()
		
//		.get("/stats")
//		.route()
//		.wireTap("controlbus:route?routeId="
//		+Constantes.RutasId.ColasBancos+"&action=stats")
//		.log("${body}")
//		.log("${headers}")
//		.setBody(constant("Se pidieron los stats de la ruta: "
//				+Constantes.RutasId.ColasBancos))
//		.to("file:logs/xml?fileExist=Append&fileName=controlBusStats")
//		.endRest()
		
		
		.get("/start")
		.route()
		.wireTap("controlbus:route?routeId="
				+Constantes.RutasId.embotelladoBBVA+"&action=start")
				.wireTap("controlbus:route?routeId="
						+Constantes.RutasId.embotelladoGalicia+"&action=start")
				.wireTap("controlbus:route?routeId="
						+Constantes.RutasId.embotelladoHSBC+"&action=start")
				.setBody(constant("Se iniciaron las rutas: "
						+Constantes.RutasId.embotelladoBBVA+" "
						+Constantes.RutasId.embotelladoHSBC+" "
						+Constantes.RutasId.embotelladoGalicia))
		.endRest();

	
rest("/queue-bdd-control")

.get("/stop")
.route()
.wireTap("controlbus:route?routeId="
+Constantes.RutasId.embotellado+"&action=stop")
.setBody(constant("Se detuvo la ruta: "
		+Constantes.RutasId.embotellado))
.endRest()

.get("/stats")
.route()
.wireTap("controlbus:route?routeId="
+Constantes.RutasId.embotellado+"&action=stats")
.log("${body}")
.log("${headers}")
.setBody(constant("Se pidieron los stats de la ruta: "
		+Constantes.RutasId.embotellado))
.to("file:logs/xml?fileExist=Append&fileName=controlBusStats")
.endRest()


.get("/start")
.route()
.wireTap("controlbus:route?routeId="
+Constantes.RutasId.embotellado+"&action=start")
.setBody(constant("Se inicio la ruta: "
		+Constantes.RutasId.embotellado))
.endRest();
	}

	
	
}
