package com.micro.camel.usuarios.rutas;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.processors.FindById;
import com.micro.camel.usuarios.processors.MensajeCuentaBorrada;
import com.micro.camel.usuarios.repository.UsuarioDao;
@Component
@DependsOn("RouteConfig")
public class DeleteRoutes extends ErrorHandlerRoute {

	
	private UsuarioDao usudao;
	
	
	private FindById FindById;
	
	
	private MensajeCuentaBorrada msjborrada;
	
    @Autowired
    public DeleteRoutes(FindById fb, UsuarioDao ua, MensajeCuentaBorrada mcb) {
    	FindById  = fb;
    	usudao = ua;
    	msjborrada = mcb;
    	
    }
	
	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub

		super.configure();
		
		rest("/usuarios")
		.delete("/{id}")
		.route()
		.process(FindById)
		.process(new Processor() {
			
			@Override
			public void process(Exchange exchange) throws Exception {
				// TODO Auto-generated method stub
				
				usudao.deleteById(Long.parseLong(exchange.getIn().getHeader("id").toString()));
				System.out.println("Cuenta Borrada");
			}
		})

		.process(msjborrada)
		.endRest();
		
		/**
		from("direct:teststart")
		.process(msjborrada)
		.to("mock:result");
		*/
	}

}
