package com.micro.camel.usuarios.rutas;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;

import com.micro.camel.usuarios.exceptions.UsuarioPasswordIncorrectoException;
import com.micro.camel.usuarios.exceptions.userNotFoundException;

public abstract class ErrorHandlerRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub

		onException(Exception.class)
		.handled(true)
		.log(LoggingLevel.ERROR, "An error occurred: ${exception.stacktrace}")
		.to("direct:catchall");
		
		onException(userNotFoundException.class)
		.handled(true)
		.log("Procesando error usuario no encontrado")
		.to("direct:catchusernotfound");
		
		onException(RuntimeException.class)
		.handled(false)
//		.handled(true)
		.log("Runtime Exception disparada")
		.to("direct:catchRuntime");
		
		onException(NumberFormatException.class)
		.handled(true)
		.log(LoggingLevel.ERROR, "An error occurred: ${exception.stacktrace}")
		.log("Number Format error, alguien puso una letra donde va un numero.")
		.to("direct:numberformat");
		
		
		onException(UsuarioPasswordIncorrectoException.class)
		.handled(true)
		.log(LoggingLevel.ERROR, "An error occurred: ${exception.stacktrace}")
		.to("direct:catchUsuarioPasswordIncorrecto");
		
		
	}

}
