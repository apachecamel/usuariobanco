package com.micro.camel.usuarios.rutas;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.model.Constantes;
import com.micro.camel.usuarios.model.ErrorMessage;

@Component
public class ErrorRoutes extends ErrorHandlerRoute {

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		super.configure();
		from("direct:catchall").log("Rutas de error invocada")
		.setBody(()-> new ErrorMessage(Constantes.Errores.ERROR_GENERICO_NO_CAPTADO))
		.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(500))
		.to("direct:marshal");
//		.marshal().json(JsonLibrary.Jackson, ErrorMessage.class);
		
		from("direct:catchusernotfound")
		.setBody(() -> new ErrorMessage(Constantes.Errores.USUARIO_NO_ENCONTRADO))
		.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(500))
		.to("direct:marshal");
		
		from("direct:catchUsuarioPasswordIncorrecto")
		.setBody(()-> new ErrorMessage(Constantes.Errores.USUARIO_PASSWORD_INCORRECTO))
		.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(500))
		.to("direct:marshal");
		
		from("direct:catchRuntime")
		.setBody(()-> new ErrorMessage(Constantes.PLACEHOLDER))
		.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(500))
		.to("direct:marshal");
		
		from("direct:numberformat")
		.setBody(()-> new ErrorMessage(Constantes.Errores.NUMBER_FORMAT_E))
		.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(500))
		.to("direct:marshal");
		
		from("direct:marshal").marshal().json(JsonLibrary.Jackson, ErrorMessage.class);
	}

}
