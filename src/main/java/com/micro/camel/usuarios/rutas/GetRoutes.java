package com.micro.camel.usuarios.rutas;



import org.apache.camel.BeanInject;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.exceptions.FormatoInvalidoException;
import com.micro.camel.usuarios.model.Usuario;
import com.micro.camel.usuarios.processors.CheckHeader;
import com.micro.camel.usuarios.processors.FindById;
import com.micro.camel.usuarios.processors.FindByName;
import com.micro.camel.usuarios.repository.UsuarioDao;

@Component
public class GetRoutes extends ErrorHandlerRoute {

	@BeanInject
	private UsuarioDao usudao;
	
	@BeanInject
	private FindById findById;
	
	@BeanInject
	private FindByName findbyname;
	
	@BeanInject
	private CheckHeader checkheader;
	
	
	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		super.configure();
		rest("/usuarios")
		
			.get().route()
				.setBody(()-> usudao.findAll())
			.log("Body del exchange: ${body}")
			.endRest()
		
			.get("/{id}")
			.consumes(MediaType.APPLICATION_JSON_VALUE).produces(MediaType.APPLICATION_JSON_VALUE)
			.type(Usuario.class).outType(Usuario.class)
			.route()
			.process(checkheader)
			.choice()
				.when(header("type").contains("Long"))
//			.doTry()
					.log("Entre por Int: ${header.id}")
					.process(findById)
				.endChoice()
				.when(header("type").contains("String"))
					.process(findbyname)
					.log("Entre por String: ${header.id}")
				.endChoice()
				.otherwise()
					.throwException(new FormatoInvalidoException())
				.endRest();
					
			
	}

}
