package com.micro.camel.usuarios.rutas;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.exceptions.UsuarioPasswordIncorrectoException;
import com.micro.camel.usuarios.model.Constantes;
import com.micro.camel.usuarios.processors.Util;

@Component
public class LoginRoutes extends ErrorHandlerRoute {

	@Autowired
	private Util util;
	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		super.configure();
		rest("/usuarios")
		
		.get("/login")
		.route()
		.log("${headers.authorization}")
		.process(new Processor() {
			
			@Override
			public void process(Exchange exchange) throws Exception {
				// TODO Auto-generated method stub
				
				javafx.util.Pair<String, String> usu_pass =
				util.intentarBasicAuth(exchange.getIn().getHeader(Constantes.Headers.AUTHORIZATION).toString());
				System.out.println("Usuario: "+usu_pass.getKey());
				System.out.println("Password: "+usu_pass.getValue());
				if(util.validar(usu_pass)) {
					String s = util.generarToken();
					System.out.println(s);
				}else {
					throw new UsuarioPasswordIncorrectoException();
				}
			}
		})
		.endRest();

	}

}
