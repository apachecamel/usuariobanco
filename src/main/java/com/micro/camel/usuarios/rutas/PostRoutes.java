package com.micro.camel.usuarios.rutas;

import org.springframework.context.annotation.DependsOn;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.apache.camel.BeanInject;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.language.Bean;
import org.apache.camel.model.DataFormatDefinition;
import org.apache.camel.spi.DataFormat;

import com.micro.camel.usuarios.model.Constantes;
import com.micro.camel.usuarios.model.Usuario;
import com.micro.camel.usuarios.processors.AddBanco;
import com.micro.camel.usuarios.processors.AddCuenta;
import com.micro.camel.usuarios.processors.SaveEntity;

@Component
@DependsOn("RouteConfig")
public class PostRoutes extends ErrorHandlerRoute {
	
	@BeanInject
	private SaveEntity processorBdd;
	
	@BeanInject
	private AddCuenta addCuenta;
	
	@BeanInject
	private AddBanco addBanco;

	private DataFormat jsonTOUsuario = new JacksonDataFormat(Usuario.class);

	
	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		super.configure();
		System.out.println("Coverage testing");
		
		rest("/usuarios")
		
		
		.post("/banco/{idbanco}")
		.consumes(MediaType.APPLICATION_JSON_VALUE).produces(MediaType.APPLICATION_JSON_VALUE)
		.type(Usuario.class).outType(Usuario.class)
		.route()
		.marshal(jsonTOUsuario)
		.wireTap(Constantes.Rutas.ColasBancos)
		.unmarshal(jsonTOUsuario)
		.endRest()
		
		.post("/deprecated/bankless/countless")
		.consumes(MediaType.APPLICATION_JSON_VALUE).produces(MediaType.APPLICATION_JSON_VALUE)
		.type(Usuario.class).outType(Usuario.class)
		.route()
		.to(Constantes.Rutas.ProcesadorBdd)
		.endRest()
		
		.post("/deprecated/banco/{bancoid}/cuenta/{cuentaid}")
		.consumes(MediaType.APPLICATION_JSON_VALUE).produces(MediaType.APPLICATION_JSON_VALUE)
		.type(Usuario.class).outType(Usuario.class)
		.route()
		.process(addCuenta)
		.process(addBanco)
		.to(Constantes.Rutas.ProcesadorBdd)
		.endRest()
		
		
		.post("/deprecated/multicast")
		.consumes(MediaType.APPLICATION_JSON_VALUE).produces(MediaType.APPLICATION_JSON_VALUE)
		.type(Usuario.class).outType(Usuario.class)
		.route()
		.routeId("Multicasting")
		.multicast().parallelProcessing()
		.to("seda:tomq",Constantes.Rutas.ProcesadorBdd,Constantes.Rutas.LOGGER)
		.endRest();
		
		from(Constantes.Rutas.ProcesadorBdd)
		.routeId(Constantes.RutasId.ProcesadorBdd)
		.process(processorBdd);
		
		

	}

}
