package com.micro.camel.usuarios.rutas;

import org.apache.camel.BeanInject;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.model.Usuario;
import com.micro.camel.usuarios.processors.FindById;
import com.micro.camel.usuarios.processors.AddBanco;
import com.micro.camel.usuarios.processors.AddCuenta;
import com.micro.camel.usuarios.processors.SaveEntity;
import com.micro.camel.usuarios.repository.UsuarioDao;
@Component
public class PutRoutes extends ErrorHandlerRoute {

	@BeanInject
	private SaveEntity processorBdd;
	
	@BeanInject
	private AddCuenta addCuenta;
	
	@BeanInject
	private AddBanco addBanco;
	
	@BeanInject
	private FindById findById;
	
	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		super.configure();
		rest("/usuarios")
		
		.put("/{id}")
		//modifica el usuario con otro por body
		.consumes(MediaType.APPLICATION_JSON_VALUE).produces(MediaType.APPLICATION_JSON_VALUE)
		.type(Usuario.class).outType(Usuario.class)
		.route()
		.process(processorBdd)
		.endRest()
		
		
		.put("/{id}/banco/{bancoid}/cuenta/{cuentaid}")
		//Agrega una cuenta de un banco
		.consumes(MediaType.APPLICATION_JSON_VALUE).produces(MediaType.APPLICATION_JSON_VALUE)
		.type(Usuario.class).outType(Usuario.class)
		.route()
		.process(findById)
		.process(addBanco)
		.process(addCuenta)
		.process(processorBdd)
		.endRest();
		
	}

}
