package com.micro.camel.usuarios.rutas;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

@Component("RouteConfig")
public class RouteConfig extends ErrorHandlerRoute {

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		super.configure();
		
		restConfiguration().component("servlet").port(9000).host("localhost").bindingMode(RestBindingMode.json)
		
		//Enable swagger endpoint.
		.apiContextPath("/swagger") //swagger endpoint path
		.apiContextRouteId("swagger") //id of route providing the swagger endpoint

		 //Swagger properties
		.contextPath("") //base.path swagger property; use the mapping path set for CamelServlet
		.apiProperty("api.title", "Usuarios de Bancos API")
		.apiProperty("api.version", "1.0");

	}

}
