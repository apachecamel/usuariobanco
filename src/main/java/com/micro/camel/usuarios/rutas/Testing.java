package com.micro.camel.usuarios.rutas;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.springframework.stereotype.Component;

import com.micro.camel.usuarios.model.Constantes;
import com.micro.camel.usuarios.model.ErrorMessage;

@Component
public class Testing extends ErrorHandlerRoute {
	
	private   static int delay=200;

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		super.configure();
		
		from("direct:start")
		.routeId("Inicio")
		.multicast(new AggregationStrategy() {
			
			@Override
			public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
				
				 if (oldExchange == null) {
			            return newExchange;
			        } else {
//			        	(body1 == null) ? body2 :
//			            String body1 = oldExchange.getIn().getBody(String.class);
//			            String body2 = newExchange.getIn().getBody(String.class);
//			            String merged =  body1 + "," + body2;
			        	ErrorMessage oldd = oldExchange.getIn().getBody(ErrorMessage.class);
			        	ErrorMessage neww = newExchange.getIn().getBody(ErrorMessage.class);
			        	ErrorMessage merged = new ErrorMessage();
			        	merged.setMessage(oldd.getMessage() + neww.getMessage());
			        	merged.setError(oldd.getError()+1);
			            oldExchange.getIn().setBody(merged);
			            return oldExchange;
			        }
			    }
			
		}) 
		.parallelProcessing()
//		.timeout(500)
		.to("direct:a", "direct:b", "direct:c")
		.timeout(500)
		.end()
		.wireTap("direct:wiretap")
		.to("direct:result");
		
		from("direct:a").routeId("A")
		.setBody(()-> new ErrorMessage("Mensaje A"))
		.log("${body.message}")
		.wireTap("direct:delay")
		.delay(delay)
		.wireTap("direct:delay")
		.log("Delay ruta A: "+delay);
		
		from("direct:b").routeId("B")
		.setBody(()-> new ErrorMessage("Mensaje B"))
		.log("${body.message}")
		.wireTap("direct:delay")
		.delay(delay)
		.wireTap("direct:delay")
		.log("Delay ruta B: "+delay);;
		
		from("direct:c").routeId("C")
		.setBody(()-> new ErrorMessage("Mensaje C"))
		.log("${body.message}")
		.wireTap("direct:delay")
		.delay(delay)
		.wireTap("direct:delay")
		.log("Delay ruta C: "+delay);
		
		from("direct:result").routeId("Resultado")
		.log("${body.message}");
		
		rest("/test").get("/ag").to("direct:start");
		
		from("direct:wiretap")
		.log("${body}")
		.setBody(constant("esto se hizo en el wiretap"))
		.setHeader("wiretap")
		.simple("Hecho en wiretap")
		.to("direct:result2");
		
		from("direct:result2").routeId("Resultado2")
		.log("${headers.wiretap}")
		.log("${body}");
		
		from("direct:delay")
		.process(new Processor() {
			
			@Override
			public final void process(Exchange exchange) throws Exception {
				// TODO Auto-generated method stub
				if (delay<1500) {
					delay = 1500;
				}else {
					delay = delay *2;
				}
				System.out.println("Proc Delay: "+delay);
				
			}
		})
		.log("Ruta delay: "+delay);
		
		from("direct:gudbai")
		.routeId("foo")
		.setBody(constant("GudBai"))
		.to("activemq:queue:greetings");
		
		rest("/timer")
		.get("/envio/start")
		.route()
		.wireTap("controlbus:route?routeId="
		+Constantes.RutasId.ActiveMqTimer+"&action=start")
		.to("log:LOGGER?showAll=true&multiline=true")
		.setBody(constant("Se inicio la ruta: "+Constantes.RutasId.ActiveMqTimer))
		.endRest()
		
		.get("/envio/stop")
		.route()
		.wireTap("controlbus:route?routeId="
		+Constantes.RutasId.ActiveMqTimer+"&action=stop")
		.to("log:LOGGER?showAll=true&multiline=true")
		.setBody(constant("Se detuvo la ruta: "+Constantes.RutasId.ActiveMqTimer))
		.endRest()
		
		.get("/recibo/start")
		.route()
		.wireTap("controlbus:route?routeId="
		+Constantes.RutasId.ActiveMqTimerReceiver+"&action=start")
		.to("log:LOGGER?showAll=true&multiline=true")
		.setBody(constant("Se inicio la ruta: "+Constantes.RutasId.ActiveMqTimerReceiver))
		.endRest()
		
		.get("/recibo/stop")
		.route()
		.wireTap("controlbus:route?routeId="
		+Constantes.RutasId.ActiveMqTimerReceiver+"&action=stop")
		.to("log:LOGGER?showAll=true&multiline=true")
		.setBody(constant("Se detuvo la ruta: "+Constantes.RutasId.ActiveMqTimerReceiver))
		.endRest();
		
		
	}

}
