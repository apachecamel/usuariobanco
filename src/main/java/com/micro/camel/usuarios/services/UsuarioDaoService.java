package com.micro.camel.usuarios.services;
import javafx.util.Pair;

public interface UsuarioDaoService {

	public String generarToken();
	public boolean validar(Pair<String, String> usu_pass)throws Exception;
	public Pair<String, String> intentarBasicAuth(String str);
}
