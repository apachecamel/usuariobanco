package com.micro.camel.usuarios.processors;




import static org.mockito.Mockito.mock;

import java.util.Optional;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.test.spring.CamelSpringTestSupport;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.micro.camel.usuarios.exceptions.userNotFoundException;
import com.micro.camel.usuarios.model.Usuario;
import com.micro.camel.usuarios.repository.UsuarioDao;

import utillitarios.ConstantesTest;

@RunWith(SpringRunner.class)
public class FindByIdTest extends CamelSpringTestSupport {

	@TestConfiguration
	static class FindByIdContextConfig{
		
		@Bean
		public FindById findById() {
			return new FindById();
		}
		
		@Bean
		public CamelContext createCamelContext() {
			return new DefaultCamelContext();
		}
	}
	
	@InjectMocks
	FindById componente;
	
	@Mock
	UsuarioDao usudao = mock(UsuarioDao.class);
	
	Usuario johan;
	
	@Autowired
	CamelContext ctx;
	
	Long id = 7L;
	
	@Mock
	ApplicationContext appctx;
	
	@Override
	protected AbstractApplicationContext createApplicationContext() {
		// TODO Auto-generated method stub
		
		return (AbstractApplicationContext) appctx;
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		johan = new Usuario(ConstantesTest.USU_NAME, ConstantesTest.USU_PASS);
		johan.setId(id);
		
	    
	}
	
	@Test
	public void testFindById_thenReturnUser() throws Exception {
		
		Mockito.when(usudao.findById(id))
	      .thenReturn(Optional.of(johan));
		
		Exchange ex = new DefaultExchange(ctx);
		ex.getIn().setHeader("id", id);
		componente.process(ex);
		assertEquals(johan.toString(), ex.getIn().getBody().toString());
	}

	@Test(expected = userNotFoundException.class)
	public void testFindByID_theReturnNotFound() throws Exception {
		
		Mockito.when(usudao.findById(id))
	      .thenReturn(Optional.empty());
		
		Exchange ex = new DefaultExchange(ctx);
		ex.getIn().setHeader("id", id);
		componente.process(ex);
	}

}
