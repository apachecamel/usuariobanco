package com.micro.camel.usuarios.processors;

import static org.junit.Assert.assertNotNull;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultExchange;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import com.micro.camel.usuarios.repository.UsuarioDao;
import com.micro.camel.usuarios.rutas.DeleteRoutes;

@EnableAutoConfiguration
@SpringBootTest
@RunWith(CamelSpringBootRunner.class)
public class MensajeCuentaBorradaTest extends CamelTestSupport{

	UsuarioDao usudao;
	
	
	FindById FindById;
	
	MensajeCuentaBorrada msjcuentaB;
	
	@Override
	protected RouteBuilder createRouteBuilder() {
		return new DeleteRoutes(FindById, usudao, msjcuentaB);
	}
	
	 @EndpointInject(uri = "mock:result")
	  private MockEndpoint resultEndpoint;

	  @Produce(uri = "direct:teststart")
	  private ProducerTemplate template;
	
	@Autowired
	CamelContext camelctx;
	
	
	 @Before
	  public void setUp() throws Exception {
	    camelctx.addRoutes(createRouteBuilder());
	    camelctx.start();
	  }
	
	
	 @DirtiesContext
	@Test
	public void test() throws Exception {
		  resultEndpoint.expectedMessageCount(1);
		    String myProcessorInput = "test"; // or Object
		    template.sendBody(myProcessorInput);

		    resultEndpoint.assertIsSatisfied();

		    // or other result Type
		    String body = (String)resultEndpoint.getExchanges().get(0).getIn().getBody(String.class);

		    assertNotNull(body);
		    // tests
		  }

}
