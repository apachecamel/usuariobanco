package com.micro.camel.usuarios.processors;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.micro.camel.usuarios.exceptions.userNotFoundException;

import com.micro.camel.usuarios.model.Usuario;
import com.micro.camel.usuarios.repository.UsuarioDao;
import com.micro.camel.usuarios.services.UsuarioDaoService;

import javafx.util.Pair;
import utillitarios.ConstantesTest;

@RunWith(value=SpringRunner.class)
public class UtilTest {

	@Autowired
	UsuarioDaoService util;
	
	@MockBean
	UsuarioDao usudao;
	

	ArrayList<Usuario> usuarios;


	@TestConfiguration
	static class UtilConfig{
		@Bean
		public UsuarioDaoService util() {
			return new Util();
		}
		
	}
	
	@Before
	public void setUp() {
		
		usuarios = new ArrayList<Usuario>();
		Usuario usu = new Usuario(ConstantesTest.USU_NAME,ConstantesTest.USU_PASS);
		usuarios.add(usu);
		Mockito.when(usudao.findByName(ConstantesTest.USU_NAME)).thenReturn(usuarios);
		
	}
	
	@Test
	public void intentarBasicAuthTest() {
		String input = ConstantesTest.BasicHeader;
		Pair<String, String> output = new Pair<String, String>(ConstantesTest.USU_NAME,ConstantesTest.USU_PASS); 
		Pair<String, String> result = util.intentarBasicAuth(input);
		assertEquals(output, result);
	}

	@Test
	public void validarTestTrue() throws Exception {
		Pair<String, String> inputTrue = new Pair<String, String>(ConstantesTest.USU_NAME,ConstantesTest.USU_PASS);
		
		boolean result = util.validar(inputTrue);

		assertEquals(true, result);
	}
	
	@Test(expected = userNotFoundException.class)
	public void validarTestException() throws Exception {
		Pair<String, String> inputFalse = new Pair<String, String>("",ConstantesTest.USU_PASS);
		
		boolean result = util.validar(inputFalse);

//		assertEquals(true, result);
	}
	@Test
	public void validarTestFalse() throws Exception {
		Pair<String, String> inputFalse = new Pair<String, String>(ConstantesTest.USU_NAME,"");
		
		boolean result = util.validar(inputFalse);

		assertEquals(false, result);
	}
	
	@Test (expected = RuntimeException.class)
	public void intentarBasicAuthTestE1() {
		String input = "BasicTWF4Om1heDMz";
		Pair<String, String> output = new Pair<String, String>("Max","max33"); 
		Pair<String, String> result = util.intentarBasicAuth(input);
//		assertEquals(output, result);
	}
	@Test (expected = RuntimeException.class)
	public void intentarBasicAuthTestE2() {
		String input = "Otracosa TWF4Om1heDMz";
		Pair<String, String> output = new Pair<String, String>("Max","max33"); 
		Pair<String, String> result = util.intentarBasicAuth(input);
//		assertEquals(output, result);
	}
	
	@Test (expected = RuntimeException.class)
	public void intentarBasicAuthTestE3() {
		String input = "Basic Sm9oYW46am9oYW41NTpvdHJhY29zYQ==";
		Pair<String, String> output = new Pair<String, String>("Max","max33"); 
		Pair<String, String> result = util.intentarBasicAuth(input);
//		assertEquals(output, result);
	}
	@Test
	public void generarTokenTest() {

		String expected = "Se genera un token. "+ConstantesTest.PLACEHOLDER;
		String result = util.generarToken();
		assertEquals(expected, result);
	}
}
