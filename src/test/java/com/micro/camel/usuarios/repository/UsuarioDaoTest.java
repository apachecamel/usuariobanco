package com.micro.camel.usuarios.repository;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.micro.camel.usuarios.model.Usuario;

import utillitarios.ConstantesTest;


@RunWith(SpringRunner.class)
@DataJpaTest
public class UsuarioDaoTest {

	@Autowired
	TestEntityManager em;
	
	@Autowired
	UsuarioDao usudao;
	
	@Test
	public void whenFindbyname_ThenReturnUsuario() {
		//given
		Usuario johan = new Usuario(ConstantesTest.USU_NAME, ConstantesTest.USU_PASS);
		em.persistAndFlush(johan);
		
		//when
		ArrayList<Usuario> encontrado = usudao.findByName(johan.getName());
		
		//then
		assertEquals(johan.getName(), encontrado.get(0).getName());
		
	}
	
	@Test
	public void whenFindbyname_thenReturnNotFound() {
		
		//given
//		Usuario johan = new Usuario(ConstantesTest.USU_NAME, ConstantesTest.USU_PASS);
//		em.persistAndFlush(johan);
			
		//when
		ArrayList<Usuario> encontrado = usudao.findByName(ConstantesTest.USU_NAME);
		
		//then
		assertEquals(0, encontrado.size());
	}

}
