package com.micro.camel.usuarios.rutas;

import java.util.Optional;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.DefaultProducerTemplate;
import org.apache.camel.test.spring.CamelSpringTestSupport;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.micro.camel.usuarios.model.ErrorMessage;
import com.micro.camel.usuarios.model.Usuario;
import com.micro.camel.usuarios.processors.FindById;
import com.micro.camel.usuarios.repository.UsuarioDao;

import utillitarios.ConstantesTest;
import utillitarios.Funciones;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class DeleteRoutesTest extends CamelSpringTestSupport {

	@TestConfiguration
	static class DeleteRoutesTestConfig{
		@Bean
		public CamelContext createCamelContext() {
			return new DefaultCamelContext();
		}
		
		@Bean
		public ProducerTemplate createProducerTemplate() {
			return new DefaultProducerTemplate(ctx);
		}
		
		@Bean
		public TestRestTemplate createTestRestTemplate() {
			return new TestRestTemplate();
		}
	}
	
	@Autowired
	private TestRestTemplate restTemp;
	
	Long id = 8L;
	String msj = "Cuenta con id: "+id+" Borrado con exito";
	int idnotfound = 1;
	ErrorMessage msjError;
	
	@Mock
	ApplicationContext appctx;
	
	@Autowired
	static
	CamelContext ctx;
	
	@Autowired
	ProducerTemplate temp;
	
	@Mock
	FindById buscarporid;
	
	@Mock
	UsuarioDao usudao;
	
	Usuario johan;
	Exchange exchange;
	
	@Before
	public void setup() throws Exception {
		MockitoAnnotations.initMocks(this);
		msjError = Funciones.setUpMensajeErrorUserNotFound();
		
	}
	
	/** 
	 * LA VAR "id" SE ENTIENDE COMO ID EXISTENTE (FLUJO NORMAL)
	 * LA VAR "idnotfound" SE ENTIENDE COMO ID INEXISTENTE (FLUJO DE EXCEPCION)
	 */
	@Test
	public void testRuta() {
	
		
	
		johan = new Usuario(ConstantesTest.USU_NAME, ConstantesTest.USU_PASS);
		johan.setId(id);

		Mockito.when(usudao.findById(id))
	      .thenReturn(Optional.of(johan));
		
//		Mockito.when(buscarporid.process(exchange)).thenReturn();
		
		ResponseEntity<String> response = restTemp.getForEntity("/usuarios/"+id, String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		String s = response.getBody();
		assertEquals(msj,s);
	}
	
	
	@Test
	public void testRutaFallida() {
//		ResponseEntity<ErrorMessage> response = restTemp.getForEntity("/usuarios/"+idnotfound, ErrorMessage.class);
//		
//		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
//		ErrorMessage em = response.getBody();
//		assertEquals(msjError.getError(),em.getError());
//		assertEquals(msjError.getMessage(),em.getMessage());
	}

	@Override
	protected AbstractApplicationContext createApplicationContext() {
		// TODO Auto-generated method stub
		return (AbstractApplicationContext) appctx;
	}

}
