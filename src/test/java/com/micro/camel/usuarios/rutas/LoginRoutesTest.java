package com.micro.camel.usuarios.rutas;

import java.util.Optional;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.spring.CamelSpringTestSupport;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.micro.camel.usuarios.model.Usuario;
import com.micro.camel.usuarios.processors.FindById;
import com.micro.camel.usuarios.repository.UsuarioDao;

@EnableAutoConfiguration
@SpringBootTest
@RunWith(SpringRunner.class)
public class LoginRoutesTest extends CamelSpringTestSupport {

	@EndpointInject(uri = "mock:result")
	protected MockEndpoint resultEndpoint;

	@Produce(uri = "direct:start")
	protected ProducerTemplate template;

	@MockBean
	UsuarioDao usudao;

	@Autowired
	FindById findbyid;

	static final String USU_NAME = "Johan";
	static final String USU_PASS = "johan55";
	Usuario user;
	Optional<Usuario> userO = Optional.empty();
	static final Long ID = (long) 7;
//		private static final CamelContext Exchange = null; 

	@TestConfiguration
	static class FindConfig {
		@Bean
		public FindById find() {
			return new FindById();
		}
	}

	@Before
	public void setUp() throws Exception {
//			org.apache.camel.impl.DefaultExchange exchange = new DefaultExchange(Exchange);
//			Mockito.when(findbyid.process(exchange))
		super.setUp();
		user = new Usuario(USU_NAME, USU_PASS);
		userO = Optional.of(user);
		Mockito.when(usudao.findById(ID)).thenReturn(userO);
	}

	@Test
	public void test() throws InterruptedException {
//		  String expectedBody = "<matched/>";

		resultEndpoint.expectedBodiesReceived(user);

		template.sendBodyAndHeader(null, "id", ID);
//	        template.sendBodyAndHeader(expectedBody, "foo", "bar");

		resultEndpoint.assertIsSatisfied();
	}

//	@Test
//    public void testSendNotMatchingMessage() throws Exception {
//        resultEndpoint.expectedMessageCount(0);
//
//        template.sendBodyAndHeader("<notMatched/>", "foo", "notMatchedHeaderValue");
//
//        resultEndpoint.assertIsSatisfied();
//    }

//	  @Override
	protected RouteBuilder createRouteBuilder() {
		return new RouteBuilder() {
			public void configure() {
				from("direct:start").process(findbyid).to("mock:result");
			}
		};

	}

	@Override
	protected AbstractApplicationContext createApplicationContext() {
		// TODO Auto-generated method stub
//		CamelContext ctx = super().createCamelContext();
//		return (AbstractApplicationContext) ctx;
		return null;
	}

//@Override
//protected AbstractXmlApplicationContext createApplicationContext() {
//	// TODO Auto-generated method stub
//	return null;
//}
}
