package com.micro.camel.usuarios.rutas;

import static org.junit.Assert.*;

import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import com.micro.camel.usuarios.model.ErrorMessage;

import utillitarios.Funciones;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@RunWith(CamelSpringBootRunner.class)
public class PostRoutesTest {
	
	@Before
	public void setUp() {
		ErrorMessage emsj = Funciones.setUpMensajeErrorUserNotFound();
	}

	/**
	 * FLUJO NORMAL
	 */
	@Test
	public void testRutaConBancoYCuenta() {
		fail("Not implemented");
	}

	@Test
	public void testRutaSinBancoNiCuenta() {
		fail("Not implemented");
	}
	/**
	 * FLUJO DE EXCEPCION
	 */
	
}
