package com.micro.camel.usuarios.rutas;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.camel.test.spring.MockEndpoints;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.micro.camel.usuarios.model.ErrorMessage;

@SpringBootTest(webEnvironment= WebEnvironment.RANDOM_PORT)
@RunWith(CamelSpringBootRunner.class)
@MockEndpoints("file:output")

public class TestingTest {


	@EndpointInject(uri = "mock:file:output")
	MockEndpoint mock;
	
	@Autowired
	ProducerTemplate template;
	
	CamelContext ctx;

	@Autowired
	TestRestTemplate restTemplate;
	
	protected CamelContext createCamelContext() throws Exception{
		return ctx;
		
	}
	@Test
	public void testMethod() throws InterruptedException {
		mock.expectedBodiesReceived("GudBai");
		
		template.sendBody("direct:gudbai",null);
		
		mock.assertIsSatisfied();
	}
	

	@Test
	public void testA() throws InterruptedException {
		int error = 3;
		String message = "Mensaje AMensaje BMensaje C";
		ErrorMessage emModelo = new ErrorMessage();
		emModelo.setError(error);
		emModelo.setMessage(message);
		
		ResponseEntity<ErrorMessage> response = restTemplate.getForEntity("/test/ag", ErrorMessage.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		ErrorMessage em = response.getBody();
		assertThat(em.equals(emModelo));
		
	}
}
