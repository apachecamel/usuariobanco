package utillitarios;

import com.micro.camel.usuarios.model.Constantes;
import com.micro.camel.usuarios.model.ErrorMessage;

public class Funciones {

	private static ErrorMessage msjError;
	
	public static ErrorMessage setUpMensajeErrorUserNotFound(){
		msjError = new ErrorMessage();
		msjError.setMessage(Constantes.Errores.USUARIO_NO_ENCONTRADO);
		return msjError;
				
	}
}
